use anyhow::Result;

#[derive(Debug, PartialEq)]
struct ProblemStatement {
    departure: u64,
    lines: Vec<u64>,
}
fn main() {
    let input = include_str!("../data/input.txt");
    println!("Part 1: {}", step1(parse_input(input).unwrap()));
    println!("Part 2: {}", step2(input));
}

fn parse_input(i: &str) -> Result<ProblemStatement> {
    let mut lines = i.lines();
    Ok(ProblemStatement {
        departure: lines
            .next()
            .ok_or_else(|| anyhow::anyhow!("Missing departure"))?
            .parse::<u64>()?,
        lines: lines
            .next()
            .unwrap()
            .split(',')
            .filter(|&c| c != "x")
            .map(|t| t.parse::<u64>().unwrap())
            .collect(),
    })
}

fn step1(p: ProblemStatement) -> u64 {
    let r = p
        .lines
        .iter()
        .map(|&l| (l - p.departure % l, l))
        .min()
        .unwrap();
    r.0 * r.1
}

fn step2(input: &str) -> i64 {
    let (remainders, moduli): (Vec<i64>, Vec<i64>) = input.lines().nth(1)
        .unwrap()
        .split(',')
        .enumerate()
        .filter_map(
            |(num, lineno)| {
                let line = str::parse::<i64>(lineno).ok()?;
                Some((line - num as i64, line))
            }
        )
        .unzip();
    chinese_remainder::chinese_remainder(&remainders, &moduli).unwrap()
}

#[cfg(test)]
mod tests {
    use super::*;

    static INPUT: &str = r#"939
7,13,x,x,59,x,31,19"#;

    #[test]
    fn test_parse() {
        assert_eq!(
            parse_input(INPUT).unwrap(),
            ProblemStatement {
                departure: 939,
                lines: vec![7, 13, 59, 31, 19],
            }
        )
    }

    #[test]
    fn test_step1() {
        assert_eq!(step1(parse_input(INPUT).unwrap()), 295)
    }

    #[test]
    fn test_step2() {
        let v_rep = vec![
            (INPUT, 1068781),
            ("0\n17,x,13,19", 3417),
            ("0\n67,7,59,61", 754018),
            ("0\n67,x,7,59,61", 779210),
            ("0\n67,7,x,59,61", 1261476),
            ("0\n1789,37,47,1889", 1202161486),
        ];
        assert_eq!(
            v_rep
                .iter()
                .map(|(i, _)| (*i, step2(i)))
                .collect::<Vec<_>>(),
            v_rep,
        )
    }
}
