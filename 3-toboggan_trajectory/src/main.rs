use toboggan_trajectory::*;

fn main() {
    let map_str = include_str!("../data/input.txt");
    // println!("{}", map_str);
    let map: Map = Map::from_bytes(map_str.as_bytes());
    // println!("{:#?}", map);
    eprintln!("First part solution = {}", map.solve((3,1)));
    let sols: i64 = vec![
        (1, 1),
        (3, 1),
        (5, 1),
        (7, 1),
        (1, 2),
    ].iter().map(|slope| map.solve(*slope)).product();
    println!("Second part solution = {}", sols);
}