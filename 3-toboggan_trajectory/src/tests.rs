#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_vec2_from_tuple() {
        // let v: Vec2 = (32, 75).into();
        let v = Vec2::from((32, 75));
        assert_eq!(v, Vec2 {x: 32, y: 75})
    }
    #[test]
    fn test_format_map() {
        let m = Map{
            size: Vec2{x: 2, y:2},
            tiles: vec!(Terrain::Tree, Terrain::Plain, Terrain::Plain, Terrain::Plain)
        };
        assert_eq!(format!("{:?}", m), "#.\n..\n")
    }

    #[test]
    fn test_normalize_pos() {
        let m = Map::new((2, 2).into());
        assert_eq!(m.normalize_pos((0, 0).into()), Some((0, 0).into()));
        assert_eq!(m.normalize_pos((1, 0).into()), Some((1, 0).into()));
        assert_eq!(m.normalize_pos((2, 0).into()), Some((0, 0).into()));
        assert_eq!(m.normalize_pos((-1, 0).into()), Some((1, 0).into()));
        assert_eq!(m.normalize_pos((-2, 0).into()), Some((0, 0).into()));
        assert_eq!(m.normalize_pos((0, -1).into()), None);
        assert_eq!(m.normalize_pos((0, 2).into()), None);
    }

    #[test]
    fn test_index() {
        let m = Map::new((3, 5).into());
        assert_eq!(m.index((0, 0).into()), Some(0));
        assert_eq!(m.index((2, 0).into()), Some(2));
        assert_eq!(m.index((0, 1).into()), Some(3));
        assert_eq!(m.index((2, 1).into()), Some(5));
    }

    #[test]
    fn test_create_map() {
        let mut map = Map::new(Vec2{x:5,y:5});
        let trees: Vec<Vec2> = vec![
            (1,2).into(),
            (2,1).into(),
            (3,3).into(),
            (1,4).into(),
            (5,1).into(),
        ];
        trees.iter().for_each(|pos| map.set(*pos, Terrain::Tree));
        println!("{:?}", map)
    }
}