use std::{fmt, iter::FromIterator, mem, vec};
use anyhow::{anyhow, Result};
use fmt::{Display};
use itertools::Itertools;

#[derive(Clone, Copy, PartialEq)]
enum TileType {
    Floor,
    Empty,
    Occupied,
}

impl Default for TileType {
    fn default() -> Self {
        Self::Floor
    }
}

impl Display for TileType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            TileType::Floor => '.',
            TileType::Empty => 'L',
            TileType::Occupied => '#',
        })
    }
}

impl fmt::Debug for TileType {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(f, "{}", match self {
            TileType::Floor => ".",
            TileType::Empty => "L",
            TileType::Occupied => "#",
        })
    }
}

impl TileType {
    fn change<T>(&self, i: T) -> TileType where
        T: IntoIterator<Item = TileType> {
            match self {
                TileType::Empty => {
                    if i.into_iter()
                        .filter(|&x| x == TileType::Occupied)
                        .count() == 0 {
                            TileType::Occupied
                        } else {
                            *self
                        }
                }
                TileType::Occupied => {
                    if i.into_iter()
                        .filter(|&x| x == TileType::Occupied)
                        .count() >= 4 {
                            TileType::Empty
                        } else {
                            *self
                        }
                }
                _ => {
                    *self
                } 
            }
        }

    fn change_step2<T>(&self, i: T) -> TileType where
        T: IntoIterator<Item = TileType> {
            match self {
                TileType::Empty => {
                    if i.into_iter()
                        .filter(|&x| x == TileType::Occupied)
                        .count() == 0 {
                            TileType::Occupied
                        } else {
                            *self
                        }
                }
                TileType::Occupied => {
                    if i.into_iter()
                        .filter(|&x| x == TileType::Occupied)
                        .count() >= 5 {
                            TileType::Empty
                        } else {
                            *self
                        }
                }
                _ => {
                    *self
                } 
            }
        }}

impl From<char> for TileType {
    fn from(c: char) -> Self {
        match c {
            '.' => TileType::Floor,
            'L' => TileType::Empty,
            '#' => TileType::Occupied,
            _ => unreachable!()
        }
    }
}

#[derive(Debug, Clone, Copy, PartialEq)]
struct Vec2 {
    x: i64,
    y: i64,
}

impl<T> From<(T, T)> for Vec2
    where T: Into<i64> {
    fn from((x, y): (T, T)) -> Self {
        Self {
            x: x.into(),
            y: y.into()
        }
    }
}

impl<T> From<Vec2> for (T, T)
    where T: From<i64> {
    fn from(v: Vec2) -> Self {
        (v.x.into(), v.y.into())
    }
}

impl FromIterator<Vec2> for Vec<(i64, i64)> {
    fn from_iter<T: IntoIterator<Item = Vec2>>(iter: T) -> Self {
        iter
            .into_iter()
            .map(|v| (v.x, v.y))
            .collect()
    }
}

#[derive(Debug, Clone, PartialEq)]
struct Map {
    size: Vec2,
    tiles: Vec<TileType>,
}

impl Display for Map {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        for i in 0..self.size.x {
            for j in 0..self.size.y {
                write!(f, "{}", self.get((i,j).into()).unwrap())?;
            }
            writeln!(f)?
        };
        Ok(())
    }
}

impl Map {
    fn new(size: Vec2) -> Self {
        let num_tiles = size.x * size.y;
        Self {
            size,
            tiles: vec![Default::default(); num_tiles as usize] 
        }
    }

    fn index(&self, pos: Vec2) -> Option<usize> {
        match (pos.x, pos.y) {
            (x, y) if (0..self.size.x).contains(&x) && (0..self.size.y).contains(&y) => Some((pos.x * self.size.y + pos.y) as usize),
            _ => None,
        }
    }

    fn set(&mut self, pos: Vec2, val: TileType) -> Result<()> {
        if let Some(index) = self.index(pos) {
            self.tiles[index] = val;
            Ok(())
        } else {
            Err(anyhow!("Invalid pos {:?} in map of size {:?}", pos, self.size))
        }
    }

    fn get(&self, pos: Vec2) -> Option<TileType> {
        Some(self.tiles[self.index(pos)?])
    }

    fn neighbors(&self, pos: Vec2) -> impl Iterator<Item = Vec2> {
        (-1..=1)
            .cartesian_product(-1..=1)
            .filter(|v| v != &(0, 0))
            .map(move |(dx,dy)| Vec2{
                x: pos.x + dx,
                y: pos.y + dy
            })
    }

    fn neighbors_step2(&self, pos: Vec2) -> impl Iterator<Item = Vec2> {
        let mut v: Vec<Vec2> = vec![];
        (-1..=1)
            .cartesian_product(-1..=1)
            .filter(|v| v != &(0, 0))
            .for_each(|(dx,dy)| {
                let mut ray = pos.clone();
                loop {
                    ray.x += dx;
                    ray.y += dy;
                    if let Some(tile) = self.get(ray) {
                        if tile == TileType::Empty || tile == TileType::Occupied {
                            v.push(ray);
                            break;
                        }
                    } else {
                        break;
                    }
                }
            });
        v.into_iter()
    }

    fn neighbor_tiles(&self, pos: Vec2) -> impl Iterator<Item = TileType> + '_ {
        self.neighbors(pos)
            .filter_map(move |v| self.get(v))
    }

    fn neighbor_tiles_step2(&self, pos: Vec2)
        -> impl Iterator<Item = TileType> + '_ {
        self.neighbors_step2(pos)
            .filter_map(move |v| self.get(v))
    }

    fn read(input: &str) -> Result<Self> {
        // Read map size
        let size = (
            input.lines().count() as i64,
            input.lines().next().ok_or_else(|| anyhow!("No lines in input !"))?.chars().count() as i64
        );
        let mut m = Self::new(size.into());
        input
            .chars()
            .filter(|&c| c != '\n')
            .zip(m.tiles.iter_mut())
            .for_each(|(x, y)| *y = x.into());
        Ok(m)
    }

    fn update_to(&self, new: &mut Map) {
        for i in 0..self.size.x {
            for j in 0..self.size.y {
                let pos = (i,j).into();
                new.set(
                    pos,
                    self.get(pos)
                        .unwrap()
                        .change(
                            self.neighbor_tiles(pos)
                        )
                    ).unwrap();
            }
        }
    }
    
    fn update_to_step2(&self, new: &mut Map) {
        for i in 0..self.size.x {
            for j in 0..self.size.y {
                let pos = (i,j).into();
                new.set(
                    pos,
                    self.get(pos)
                        .unwrap()
                        .change_step2(
                            self.neighbor_tiles_step2(pos)
                        )
                    ).unwrap();
            }
        }
    }
}

fn step1(input: &str) -> usize {
    let mut m = Map::read(input).unwrap();
    let mut n = m.clone();
    let mut i = 0;
    loop {
        i += 1;
        println!("Stage {}", i);
        mem::swap(&mut m,&mut n);
        m.update_to(&mut n);
        // println!("{}", &m);
        // println!("{}", &n);
        // Naive, as update_to could also report nb of changes
        if n == m {
            break;
         }
    };
    m.tiles.into_iter().filter(|&c| c == TileType::Occupied).count()

}

fn step2(input: &str) -> usize {
    let mut m = Map::read(input).unwrap();
    let mut n = m.clone();
    let mut i = 0;
    loop {
        i += 1;
        println!("Stage {}", i);
        mem::swap(&mut m,&mut n);
        m.update_to_step2(&mut n);
        // println!("{}", &m);
        // println!("{}", &n);
        // Naive, as update_to could also report nb of changes
        if n == m {
            break;
         }
    };
    m.tiles.into_iter().filter(|&c| c == TileType::Occupied).count()

}
fn main() {
    let input = include_str!("../data/input.txt");
    dbg!(step1(input));
    dbg!(step2(input));
    }

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_phase1() {
        let input = r"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL";
        assert_eq!(step1(input), 37)
    }

    #[test]
    fn test_read() {
        let input = r"L.LL.LL.LL
LLLLLLL.LL
L.L.L..L..
LLLL.LL.LL
L.LL.LL.LL
L.LLLLL.LL
..L.L.....
LLLLLLLLLL
L.LLLLLL.L
L.LLLLL.LL
";
        let m = Map::read(input).unwrap();
        assert_eq!(m.index((0,1).into()).unwrap(), 1);
        assert_eq!(m.get((0,1).into()).unwrap(), TileType::Floor);
        assert_eq!(format!("{}",m), input);
    }

    #[test]
    fn test_neighbors() {
        let m = Map::new(Vec2 { x: 5, y: 5 });
        let n = m.neighbors((1,1).into());
        assert_eq!(
            n.collect::<Vec<(_,_)>>(),
            vec![
                (0,0),
                (0,1),
                (0,2),
                (1,0),
                (1,2),
                (2,0),
                (2,1),
                (2,2),
            ]
        );
    }
}