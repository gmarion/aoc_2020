use rain_risk::*;


#[test]
fn test_step1() {
    let input = "F10\nN3\nF7\nR90\nF11";
    assert_eq!(step1(input), 25);
}

#[test]
fn test_step2() {
    let input = "F10\nN3\nF7\nR90\nF11";
    assert_eq!(step2(input), 286);
}