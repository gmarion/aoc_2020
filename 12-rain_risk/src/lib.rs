

use program::{Direction, Instruction, parse_program};

pub mod program;

struct Ship {
    position: (i64, i64),
    direction: program::Direction,
    wpt: (i64, i64),
}

impl Default for Ship {
    fn default() -> Self {
        Self {
            position: (0, 0),
            direction: Direction::East,
            wpt: (10, 1),
        }
    }
}

impl Ship {
    fn execute(&mut self, i: &Instruction) {
        // eprintln!("Executing {}: previously at {:?}", i, self.position);
        match i {
            Instruction::Direction(d, e) => {
                match d {
                    Direction::North => {
                        self.position.1 += e;
                    }
                    Direction::South => {
                        self.position.1 -= e;
                    }
                    Direction::East => {
                        self.position.0 += e;
                    }
                    Direction::West => {
                        self.position.0 -= e;
                    }
                }
            }
            Instruction::Rotation(r, e) => {
                for _ in 0..e/90 {
                    self.direction = match r {
                        program::Rotation::Left => {
                            match self.direction {
                                Direction::North => {Direction::West}
                                Direction::East => {Direction::North}
                                Direction::South => {Direction::East}
                                Direction::West => {Direction::South}
                            }
                        }
                        program::Rotation::Right => {
                            match self.direction {
                                Direction::North => {Direction::East}
                                Direction::East => {Direction::South}
                                Direction::South => {Direction::West}
                                Direction::West => {Direction::North}
                            }
                        }
                    }
                }
            }
            Instruction::Forward(e) => {
                match self.direction {
                    Direction::North => {
                        self.position.1 += e;
                    }
                    Direction::East => {
                        self.position.0 += e;
                    }
                    Direction::South => {
                        self.position.1 -= e;
                    }
                    Direction::West => {
                        self.position.0 -= e;
                    }
                }
            }
        }
        // eprintln!("Now at {:?}", self.position);
    }

    // Execute for step2
    fn execute2(&mut self, i: &Instruction) {
        // eprintln!("Executing {}: previously at {:?}", i, self.position);
        match i {
            Instruction::Direction(d, e) => {
                match d {
                    Direction::North => {
                        self.wpt.1 += e;
                    }
                    Direction::South => {
                        self.wpt.1 -= e;
                    }
                    Direction::East => {
                        self.wpt.0 += e;
                    }
                    Direction::West => {
                        self.wpt.0 -= e;
                    }
                }
            }
            Instruction::Rotation(r, e) => {
                for _ in 0..e/90 {
                    // Don't count on me to XOR everything to avoid
                    // copies, cheers !
                    let wpt = self.wpt;
                    match r {
                        program::Rotation::Left => {
                            self.wpt.0 = -wpt.1;
                            self.wpt.1 = wpt.0;
                        }
                        program::Rotation::Right => {
                            self.wpt.0 = wpt.1;
                            self.wpt.1 = -wpt.0;
                        }
                    }    
                }
            }
            Instruction::Forward(e) => {
                self.position.0 += e * self.wpt.0;
                self.position.1 += e * self.wpt.1;
            }
        }
        // eprintln!("Now at {:?}", self.position);
    }
    pub fn manhattan(&self) -> u64 {
        (self.position.0.abs() + self.position.1.abs()) as u64
    }
}

pub fn step1(input: &str) -> u64 {
    // Parse input into path
    let p = parse_program(input).unwrap();
    // Setup ship
    let mut s = Ship::default();
    // Execute ops
    p.iter().for_each(|i| s.execute(i));
    s.manhattan()
}

pub fn step2(input: &str) -> u64 {
    // Parse input into path
    let p = parse_program(input).unwrap();
    // Setup ship
    let mut s = Ship::default();
    // Execute ops
    p.iter().for_each(|i| s.execute2(i));
    s.manhattan()
}