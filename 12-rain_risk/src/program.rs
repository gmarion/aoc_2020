use std::{fmt::{Display}, str::FromStr};

use anyhow::Error;

#[derive(Debug, PartialEq)]
pub enum Direction {
    North,
    East,
    South,
    West,
}

impl Display for Direction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Direction::North => {'N'}
            Direction::South => {'S'}
            Direction::East => {'E'}
            Direction::West => {'W'}
        })
    }
}

#[derive(Debug, PartialEq)]
pub enum Rotation {
    Left,
    Right,
}

impl Display for Rotation {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", match self {
            Rotation::Left => {'L'}
            Rotation::Right => {'R'}
        })
    }
}

#[derive(PartialEq)]
pub enum Instruction {
    Direction(Direction, i64),
    Rotation(Rotation, i64),
    Forward(i64),
}

impl FromStr for Instruction {
    type Err = anyhow::Error;

    fn from_str(s: &str) -> Result<Self, Self::Err> {
        let mut sc = s.chars();
        match sc.next().ok_or_else(|| anyhow::anyhow!("Expected NSEWLRF"))? {
            d @ 'N' | d @ 'S' | d @ 'E' | d @ 'W' => {
                Ok(Instruction::Direction(
                    match d {
                        'N' => Direction::North,
                        'S' => Direction::South,
                        'E' => Direction::East,
                        'W' => Direction::West,
                        _ => unreachable!() // Somehow needed ??
                    },
                    sc.collect::<String>().parse::<i64>()?)
                )
            },
            r @ 'L' | r @ 'R' => {
                Ok(Instruction::Rotation(
                    match r {
                        'L' => Rotation::Left,
                        'R' => Rotation::Right,
                        _ => unreachable!() // Somehow needed ?
                    },
                    sc.collect::<String>().parse::<i64>()?))
            },
            'F' => {
                Ok(Instruction::Forward(sc.collect::<String>().parse::<i64>()?))
            },
            e  => Err(anyhow::anyhow!("Expected NSEWLRF, got {}", e)), 
        }
    }
}

impl Display for Instruction {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Instruction::Direction(d, e) => {
                write!(f, "{}{}", d, e)
            },
            Instruction::Rotation(r, e) => {
                write!(f, "{}{}", r, e)
            },
            Instruction::Forward(e) => {write!(f, "F{}", e)},
        }
    }
}

pub type Program = Vec<Instruction>;

pub fn parse_program(input: &str) -> Result<Program, Error> {
    Ok(input.lines()
        .map(|l| l.parse::<Instruction>())
        .collect::<Result<_,_>>()?)
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_readwrite() {
        let input = r"F10
N3
F7
R90
F11";
        assert_eq!(
            input
                .lines()
                .map(|l| l.parse::<Instruction>().unwrap())
                .map(|i| i.to_string())
                .collect::<Vec<String>>()
                .join("\n"),
            input
        )
    }
}