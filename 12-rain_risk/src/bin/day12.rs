use rain_risk::{step1, step2};

fn main() {
    let input = include_str!("../../data/input.txt");
    println!("Step 1 : {}", step1(input));
    println!("Step 2 : {}", step2(input));
}