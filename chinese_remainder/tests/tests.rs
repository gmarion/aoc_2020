#[cfg(test)]
mod tests {
    use chinese_remainder::{EGCD, egcd, chinese_remainder};

    #[test]
    fn test_egcd() {
        assert_eq!(
            egcd(25, 7),
            EGCD {
                a: 25,
                b: 7,
                x: 2,
                y: -7,
                gcd: 1,
            }
        )
    }
    #[test]
    fn test_cr1() {
        assert_eq!(
            chinese_remainder(&[2, 3, 2], &[3, 5, 7]), Some(23));
    }

    #[test]
    fn test_cr2() {
        assert_eq!(
            chinese_remainder(&[11,22,19], &[10,4,9]), None);
    }

    #[test]
    fn test_cr3() {
        assert_eq!(
            chinese_remainder(&[10,4,12], &[11,12,13]), Some(1000));
    }

}
