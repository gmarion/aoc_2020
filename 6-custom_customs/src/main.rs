use std::{collections::HashSet, todo};

use itertools::Itertools;

fn count_scores(input: &str) -> Vec<usize> {
    input
        .split("\n\n")
        .map(|l| l.chars()
                       .unique()
                       .filter(|&c| c != '\n')
                       .count())
        .collect()
}

fn count_scores_v2(input: &str) -> Vec<usize> {
    input
    .split("\n\n")
    .map(|group| {
        group
            .lines()
            .map(|l| l.chars().collect::<HashSet<char>>())
            .fold1(|acc: HashSet<char>, hset| {
                acc.intersection(&hset).cloned().collect()
            })
            .unwrap()
            .len()
    })
    .collect()
}

fn main() {
    let input = include_str!("../data/input.txt");
    let scores = count_scores(input);
    println!("Sum of scores : {}", scores.iter().sum::<usize>());
    println!("Sum of scores v2 : {}", count_scores_v2(input).iter().sum::<usize>())
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn test_count_scores() {
        let input = r#"abc

a
b
c

ab
ac

a
a
a
a

b"#;
        assert_eq!(count_scores(input).iter().sum::<usize>(), 11)
    }

    #[test]
    fn test_count_scores_v2() {
        let input = r#"abc

a
b
c

ab
ac

a
a
a
a

b"#;
        assert_eq!(count_scores_v2(input).iter().sum::<usize>(), 6);
    }
}